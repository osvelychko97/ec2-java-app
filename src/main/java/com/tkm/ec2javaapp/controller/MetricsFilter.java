package com.tkm.ec2javaapp.controller;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public class MetricsFilter {
    private Counter counter;

    public void increaseCounter(String customMetricsName, String status, String method, String uri, MeterRegistry registry) {
        Counter counter = Counter.builder(customMetricsName)
                .tags("request", status)
                .tag("method", method)
                .tag("uri", uri)
                .description("Number of request - " + status)
                .register(registry);
        counter.increment();
    }

    public void increaseCounter(){
        counter.increment();
    }
}
