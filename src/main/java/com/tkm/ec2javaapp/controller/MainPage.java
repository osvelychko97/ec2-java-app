package com.tkm.ec2javaapp.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Slf4j
public class MainPage {
    private static final String INDEX = "index";

    @GetMapping(value = "/")
    public String getMainPage() {
        log.info("Main page load.");
        return INDEX;
    }
    @GetMapping(value = "/aaa")
    public String getMainPage2() {
        log.info("Main page load.");
        return INDEX;
    }
    @GetMapping(value = "/bbb")
    public String getMainPage3() {
        log.info("Main page load.");
        return INDEX;
    }
    @GetMapping(value = "/ccc")
    public String getMainPage4() {
        log.info("Main page load.");
        return INDEX;
    }

}
