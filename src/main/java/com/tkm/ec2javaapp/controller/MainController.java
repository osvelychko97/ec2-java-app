package com.tkm.ec2javaapp.controller;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import io.micrometer.core.instrument.MeterRegistry;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;

@RestController
public class MainController {
    private static final String S3_CONTENT = "./s3Content.txt";
    private static final String MY_CUSTOM_REQUEST_COUNTER_METRIC = "my_custom_request_counter_metric";
    private static final String STATUS_200 = "200";
    private static final String STATUS_201 = "201";
    private static final String STATUS_300 = "300";
    private static final String STATUS_400 = "400";
    private static final String STATUS_401 = "401";
    private static final String STATUS_404 = "404";
    private static final String STATUS_500 = "500";
    private static final String GET_METHOD = "GET";

    @Autowired
    private MeterRegistry registry;

    @Autowired
    private MetricsFilter metricsFilter = new MetricsFilter();

    @GetMapping(value = "/200")
    public ResponseEntity<?> get200Status() {
        metricsFilter.increaseCounter(MY_CUSTOM_REQUEST_COUNTER_METRIC, STATUS_200, GET_METHOD, "/200", registry);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/201")
    public ResponseEntity<?> get201Status() {
        metricsFilter.increaseCounter(MY_CUSTOM_REQUEST_COUNTER_METRIC, STATUS_201, GET_METHOD, "/201",registry);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/300")
    public ResponseEntity<?> get300Status() {
        metricsFilter.increaseCounter(MY_CUSTOM_REQUEST_COUNTER_METRIC, STATUS_300, GET_METHOD,"/300",registry);
        return new ResponseEntity<>(HttpStatus.MULTIPLE_CHOICES);
    }

    @GetMapping(value = "/400")
    public ResponseEntity<?> get400Status() {
        metricsFilter.increaseCounter(MY_CUSTOM_REQUEST_COUNTER_METRIC, STATUS_400, GET_METHOD, "/400",registry);
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping(value = "/401")
    public ResponseEntity<?> get401Status() {
        metricsFilter.increaseCounter(MY_CUSTOM_REQUEST_COUNTER_METRIC, STATUS_401, GET_METHOD, "/401",registry);
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @GetMapping(value = "/404")
    public ResponseEntity<?> get404Status() {
        metricsFilter.increaseCounter(MY_CUSTOM_REQUEST_COUNTER_METRIC, STATUS_404, GET_METHOD,"/404", registry);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/500")
    public ResponseEntity<?> get500Status() {
        metricsFilter.increaseCounter(MY_CUSTOM_REQUEST_COUNTER_METRIC, STATUS_500, GET_METHOD,"/500", registry);
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping(value = "/checkS3Content/{bucketName}/{fileName}")
    public ResponseEntity<?> checkS3Content(@PathVariable String bucketName,
                                            @PathVariable String fileName) throws IOException {

        BasicAWSCredentials awsCreds = new BasicAWSCredentials(
                "AKIA3EK5LNHBE4KTQZ3G",
                "QHJSgeGpvVNEzm6IkT1/Zreel26hzkPWHunjwZZb"
        );

        AmazonS3 s3client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(Regions.EU_WEST_2)
                .build();

        S3Object s3object = s3client.getObject(bucketName, fileName);
        S3ObjectInputStream inputStream = s3object.getObjectContent();

        FileUtils.copyInputStreamToFile(inputStream, new File(S3_CONTENT));

        String contentS3 = FileUtils.readFileToString(new File(S3_CONTENT));
        System.out.println("Bucket : " + bucketName);
        System.out.println("File: " + fileName);
        System.out.println("Content : " + contentS3);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
