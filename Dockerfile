FROM openjdk:8
COPY /target/java-app.jar /usr/app/
WORKDIR /usr/app
VOLUME /logs
RUN sh -c 'touch java-app.jar'
ENTRYPOINT ["java","-jar","java-app.jar"]