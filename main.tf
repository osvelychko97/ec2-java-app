variable "region" {
  default = "eu-west-2"
}

provider "aws" {
  access_key = "AKIA3EK5LNHBE4KTQZ3G"
  secret_key = "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"
  region = var.region
}

resource "aws_instance" "aws_instance" {
  ami = "ami-04a26b5fb882a2eb2"
  instance_type = "t2.micro"
  key_name = "aws_key"

  vpc_security_group_ids = [
    aws_security_group.linux_instance.id]

  user_data = "${file("app-cloud-config.yaml")}"

//  iam_instance_profile = "aws_iam_instance_profile.ec2_profile.name"

  iam_instance_profile = "${aws_iam_instance_profile.ec2_profile.name}"


  tags = {
    Name = "Spring app"
  }
}


resource "aws_iam_role" "ec2_s3_access_role" {
  name = "myCustomRoleForEC2CoreOS"
  description = "myCustomRoleForLambda-AllAccess"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "lambda_s3_policy" {
  name = "s3BucketForLambdaFullAccess"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_policy_attachment" "lambda_s3_bucket" {
  name       = "lambda_s3_bucket"
  roles      = ["${aws_iam_role.ec2_s3_access_role.name}"]
  policy_arn = "${aws_iam_policy.lambda_s3_policy.arn}"
}

resource "aws_iam_instance_profile" "ec2_profile" {
  name  = "test_profile"
  role = "${aws_iam_role.ec2_s3_access_role.name}"

}

terraform {
  backend "s3" {
    access_key = "AKIA3EK5LNHBE4KTQZ3G"
    secret_key = "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"
    region = "eu-west-2"
    bucket = "lambda-s3-state"
    key = "ec2.tfstate"
  }
}

resource "aws_security_group" "linux_instance" {
  name = "myCustomSecurityGroup"
  description = "my linux SecurityGroup"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    from_port = 19531
    to_port = 19531
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  ingress {
    from_port = 0
    to_port = 0
    protocol = "icmp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  egress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  tags = {
    Name = "Spring app"
  }
}